# Self-Description Lifecycle

Since Self-Descriptions are protected by cryptographic signatures, they are immutable and cannot be changed once published.
The lifecycle state is therefore managed outside of the Self-Description itself, like for example and not limited to, Catalogues.

Furthermore, Self-Descriptions do not have a lifecycle state that is globally agreed across Participants and Federations.
Instead, the lifecycle is computed locally based on the available information and the Trust Anchors in the local context.
The rules for deriving the lifecycle state are given in the next sections.

The individual lifecycle considerations for the underlying Entities (e.g., Service Offerings) are out of scope for this section.

## Lifecycle States

The lifecycle of the Self-Description depends on the lifecycle of the credentials contained within it.
Furthermore, both depend on the lifecycle status of the certificate (public/private key-pair) used to sign it.
The following table shows the lifecycle states for Self-Descriptions, Credentials and certificates.
All are described in more detail below.

| State            | Self-Description | Credential | Certificate |
|------------------|------------------|------------|-------------|
| Active           | x                | x          | x           |
| Partially Active | x                | –          | –           |
| Inconsistent     | x                | x          | –           |
| Unverifiable     | x                | x          | x           |
| Expired          | x                | x          | x           |
| Deprecated       | x                | –          | x           |
| Revoked          | x                | x          | x           |

Active: The Self-Description has passed all tests.

Partially Active: Some of the Verifiable Credentials inside the Self-Description are not active.
Still, the remaining Verifiable Credentials together from a valid Self-Description.

Inconsistent: The Self-Description is internally inconsistent or it does not match with previously published Self-Descriptions or the Self-Description Schemas.

Unverifiable: It was not possible to establish trust in the Self-Description (Verifiable Presentation) or at least one of the contained Claims (Verifiable Claims), which have been deemed as essential for the local verifying entity.

Expired: The Self-Descriptions are the source information for the Gaia-X Catalogue.
In order for the Catalogue to contain only up-to-date information it should be “self-cleaning”, whereby outdated information is removed automatically.
This can be achieved by timeout dates attached to every Self-Description after which they are considered to be expired.
It is recommended that the automatic timeout date of Self-Descriptions is set rather low, e.g., 90 days.
This has proven useful in the context of TLS certificates (e.g., Let's Encrypt) where a frequent renewal forces providers to put automated updates in place instead of infrequent manual updates.

Deprecated: If two versions of Gaia-X Resources (Services, Data, etc.) are offered at the same time, then each version is described by an independent Self-Description with a unique identifier.
If the Self-Description itself requires an update, the entire JSON-LD document is published in an updated version and the old Self-Description is deprecated with reference to the updated Self-Description.
As the deprecated Self-Description itself is signed and immutable, it is important to note that deprecation can not be added to the Self-Description itself, but is managed as external metadata.

Revoked: Gaia-X needs to protect against bad actors that might not be able or willing to correct false information.
Hence a revocation mechanism is put into place by which Self-Descriptions can be marked as non-active due to revocation.
Revocation can be performed by the original Issuer of a Self-Description and also by trusted third parties.
Each Catalogue decides for itself which Issuers of revocation messages are trusted.

## Computing the Self-Description Lifecycle State

A range of verification tests is defined for Self-Descriptions and Claims.
Each of the verification tests returns a lifecycle state.
Afterwards, the results from the tests are aggregated into an overall lifecycle state.
The following total order of the lifecycle states is used in the definition of the aggregation rules.
The total order does not mean that the lifecycle has to step through the states in this order.

    Active < Partially Active < Inconsistent < Unverifiable < Expired < Deprecated < Revoked
    
The verification rules are designed such that the eventual lifecycle state does not depend on the order in which these verification rules are evaluated. The following constraints are defined for the order in which the Verification Rules are to be executed. The goal is to always ensure that the input for a verification rule has itself been verified prior.

### Verification Order within a Self-Description
First the certificates used for (and within) a Self-Description are verified.
Then for each Self-Description first the individual Verifiable Credentials are verified before the Self-Description as a whole.
That way, the verification rules for the Verifiable Credentials can refer to the state and content of the used certificates.
And the verification rules for Self-Descriptions can rely on the state and content of both the certificates and the Verifiable Credentials from this Self-Description.

### Verification Order of Self-Descriptions
The lifecycle state of a Self-Description may depend on the consistency with other Self-Descriptions that are referenced.
But there is no "natural order" in which the Self-Descriptions ought to be processed.
For example, a Self-Descripion `A` may depend on Self-Description `B` that already exists.
But when `B` gets replaced by a new version `B'`, then suddenly `A` depends on a Self-Description with a later release-date.

In order to get around this, we mandate that Self-Descriptions form a "directed acyclic graph" (DAG) with input-output edges from the references used for the mandatory attributes.
Then a topological sort algorithm (e.g. Kahn's Algorithm, 1962) can be used to bring the Self-Descriptions into an absolute ordering for processing where all input information to verify a Self-Descriptions have been processed prior.
(This still allows for parallel processing of the verification rules for additional performance -- unless there is currently no Self-Description with all inputs ready.)

If Self-Descriptions are detected to form a cycle in their mandatory references, then the newest Self-Description from the cycle is set to the `inconsistent` state to break the cycle before continuing the verification.

If optional attributes point to a missing (or non-active) Self-Description, then the reference is retained for querying but not considered for the verification.
This is done to prevent "cascading effects" in case a Self-Descriptions expires or otherwise becomes non-active.

## Verification Rules for Self-Descriptions

### Syntax and Semantics

For some the verification rules, the Self-Description is “flattened” to a set of claims.
This claim set contains only claims from the Verifiable Credentials in the `Active` state.

Any catalogue maintains a set of _schemas_.  We use this term in a broad sense, including
- ontologies (defining classes and attributes),
- shapes (defining validation rules), as well as
- controlled vocabularies (defining frequently used instances of classes).
The union of all schemas in a catalogue is the reference point for any syntactic and semantic validation.

| Rule ID | Input | Rule Statement |
|-|-|-|
| SD-Claim-01 | (potential) Self-Description | *IF* the given input is not an instance of the JSON-LD grammar[^json-ld]<br>*THEN* Return `unverifiable` |
| SD-Claim-02 | (potential) Self-Description that has passed _SD-Claim-01_ | *IF* the given input is not a valid instance of the Verifiable Credentials Data Model[^vc-data-model]<br>*THEN* Return `unverifiable` |
| SD-Claim-04 | Claim set | *IF* there exists a claim _(subject, predicate, object)_ in the claim set<br>such that its _predicate_ or _object_ is not defined in any schema in the catalogue<br>*THEN* Return `inconsistent` |
| SD-Claim-05 | Claim set | Treat the claim set as a _data graph_, and the union of all schemas in the catalogue as a _shapes graph_ according to the SHACL specification[^shacl-validation].<br>*IF* this validation does not report conformance<br>*THEN* Return `inconsistent` |

[^json-ld]: JSON-LD 1.1. A JSON-based Serialization for Linked Data. W3C Recommendation 16 July 2020. https://www.w3.org/TR/json-ld11/
[^vc-data-model]: Verifiable Credentials Data Model v1.1. W3C Recommendation 03 March 2022. https://www.w3.org/TR/vc-data-model/
[^shacl-validation]: Shapes Constraint Language (SHACL).  W3C Recommendation 20 July 2017.  Section 3.4 “Validation”.  https://www.w3.org/TR/shacl/#validation-definition

### Cryptography and Trust

| Rule ID | Input | Rule Statement |
|---|---|---|
| SD-Crypto-01 | Input Self-Descriptions (+ their state) that are referenced from this Self-Description | **Case 1:**<br>The "providedBy" attribute of a Self-Description points to the Self-Description of a Gaia-X Participant<br>*AND*<br>The Self-Description for that Participant is in the `active` state<br>*THEN*<br>Return `active`<br><br>**Default Case:**<br>Return `unverifiable` |

- Every possible Self-Description schema (except the schema for Participants) defines a mandatory “providedBy” attribute.
  - The "providedBy" attribute of a Self-Description must point to the Self-Description of a Gaia-X Participant.
  - There must be an active Self-Description for that Participant already in the Catalogue.
  - Otherwise this rule returns the `Unverifiable` state
- Verify that a proof (i.e., a signature) created by the provider is present on the overall Verifiable Presentation (the outer structure of every Self-Description). ⇒ Otherwise return `Unerifiable`
- Verifiy that the Self-Description was not altered by verifying the hash in its signature ⇒ Otherwise return `Unverifiable`
- Verify that the Self-Description was signed by the Participant mentioned in the "providedBy" attribute (except for Self-Descriptions about Participants) ⇒ Otherwise return `Unverifiable`
- Verify that the certificate of the issuer is in the `Active` state ⇒ Otherwise return `Unverifiable`

### Other

- If one of the Verifiable Credentials inside a Self-Description is not in the `Active` state ⇒ Partially Active
- If the Issuer date of the Self-Description is more than X days in the past ⇒ Expired

## Verification Rules for Verifiable Credentials

### Syntax and Semantics

(See _SD-Claim-02_ above)
<!-- Christoph Lange: I think it would not make sense to specify dedicated rules for checking syntax and semantics of VC only down here.
     The syntactic and semantic validity of a SD w.r.t. the VC Data Model is a prerequisite for even being able to validate claims. -->

### Cryptography and Trust

- Verify that the VC was signed by an Issuer that is a Participant on the local trust list ⇒ Otherwise return `Unverifiable`
- Verify that the certificate of the issuer is in the `Active` state ⇒ Otherwise return `Unverifiable`

### Other

## Verification Rules for Certificates

Certificates are standalone in that their lifecycle state does not depend on credentials or self-descriptions.
The lifecycle of a certificate does however depend on what we call the *PKI Context* (Public Key Infrastructure).
The PKI Context contains lists of certificates.

- Trustlist: Contains certificates that are trusted without relying on an issuer in ther certificate chain.
- Revocation List: Certificates that are untrusted even though they may have been trusted in the path.
- Authority List: Certificates that can verify the validity of a certificate as part of a trust chain.

Where the PKI Context is stored and managed (for example in the GAIA-X Registry) is outside of the scope for this document.

### Syntax and Semantics

| Rule ID | Input | Rule Statement |
|---|---|---|
| Cert-Syntax-01 | Certificate | *IF*<br>The Certificate is in the x509v3 format according to the rules of RFC5280.<br>*AND*<br>The Certificate is encoded as a PEM (base64) string.<br>*THEN*<br>The rules returns `active`.<br>*ELSE*<br>The rule returns `unverifiable` |
| Cert-Syntax-02 | Certificate | *IF*<br>The Certificate uses the signature algorithm `sha256` and contains the public key for RSA with a key-length of at least 2048 bit.<br>*THEN*<br>The rule returns `active`.<br>*ELSE*<br>The rule returns `unverifiable`. |

### Cryptography and Trust

| Rule ID | Input | Rule Statement |
|---|---|---|
| Cert-Crypto-01 | Certificate, PKI Context | *IF*<br>The Certificate is contained in the trust list (of the PKI Context).<br>*OR*<br>The Certificate contains an x509 trust chain where all signatures in the trust chain come from certificates in the trust list.<br>*THEN*<br>The rule returns `active`.<br>*ELSE<br>The rule returns `unverifiable`. |
| Cert-Crypto-02 | Certificate, PKI Context | *IF*<br>The Certificate is not contained in the revocation list (of the PKI Context).<br>*THEN*<br>The rule returns `active`.<br>*ELSE*<br>The rule returns `unverifiable`. |

### Other

| Rule ID | Input | Rule Statement |
|---|---|---|
| Cert-Other-01 | Certificate | *IF*<br>The Certificate is within the validity period defined within the certificate itself.<br>*AND*<br>The validity period must not start more than 4 weeks into the future.<br>*AND*<br>The validity period must not be longer than 24 months.<br>*THEN*<br>The rule returns `active`.<br>*ELSE<br>The rule returns `expired`. |

## Where and by whom are the rules executed?

## Schema Lifecycle

Self-Description Schemas are a collection of files that define possible attributes and relations of the Entities in Self-Descriptions.
A Self-Description Schema is a collection of files. An ontology (mandatory), as well as SHACL shape and controlled dictionary (both optional).
Schemas are versioned.
The version number is part of the URL identifier.
Below is an example for the identifier and structure of a schema.

- Schema Identifier: http://example.org/service-schema/v1.0
- Ontology File: http://example.org/service-schema/v1.0/ontology.owl
- SHACL Shape File: http://example.org/service-schema/v1.0/shapes.shacl
- Controlled Dictionary File: http://example.org/service-schema/v1.0/vocabulary.skos

Omitting the version number refers to the latest version.
For example http://example.org/service-schema/ontology.owl.

The schema version is split into a major and minor version "<major>.<minor>".
The major and minor numbers are integer values that can be larger than nine.

Versions with a leading zero are considered work-in-progress.
There is no requirement for backwards compatibility among thos versions.
Major versions higher than zero are considered stable.
Newer minor versions for the same major version must be backwards compatible.
That is, any valid Self-Description for the previous version must remain valid.

The backwards compatibility allows to "auto-upgrade" a Self-Description to a newer schema version when the content is loaded into a Catalogue.
That way different schema versions can coexist in a recursive dependency-graph.
For example when a Self-Description `a` depends both on schema `X` and `Y` being defined in different federations.
Now when `X` and `Y` depend on different versions of some base-schema `A` this can be handled gracefully.
However, it is not possible to have different major versions of the same schema in the dependency graph of the same Self-Description.
The verification of the Self-Description Graph only applies to single Self-Descriptions.
The verification is cut off at the boundary of references between Self-Descriptions.

A Self-Description is never revoked or deprecated.
However, an old version may fall out of use and be removed individually by a Catalogue.
